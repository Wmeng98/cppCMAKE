#include "../include/student.h"

using namespace std;

// {} in MIL works fine with g++ -std=c++11 compiler but not older compilers...
student::student(string nm): name{nm} {}

string student::getName() {
	return this->name;
}

